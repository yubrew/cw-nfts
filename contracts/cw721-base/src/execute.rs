use serde::de::DeserializeOwned;
use serde::Serialize;

use cosmwasm_std::{attr, BankMsg, Binary, Coin, CosmosMsg, Deps, DepsMut, Env, MessageInfo, Response, StdResult, Uint128};
use cosmwasm_bignumber::{Uint256};

use cw2::set_contract_version;
use cw721::{ContractInfoResponse, CustomMsg, Cw721Execute, Cw721ReceiveMsg, Expiration};

use moneymarket::querier::query_balance as mm_query_balance;

use crate::error::ContractError;
use cw721::Cw721Query;
use crate::msg::{ExecuteMsg, InstantiateMsg, MintMsg};
use crate::state::{Approval, Cw721Contract, State, STATE, TokenInfo};

// version info for migration info
const CONTRACT_NAME: &str = "crates.io:cw721-base";
const CONTRACT_VERSION: &str = env!("CARGO_PKG_VERSION");

impl<'a, T, C> Cw721Contract<'a, T, C>
where
    T: Serialize + DeserializeOwned + Clone,
    C: CustomMsg,
{
    pub fn instantiate(
        &self,
        deps: DepsMut,
        _env: Env,
        _info: MessageInfo,
        msg: InstantiateMsg,
    ) -> StdResult<Response<C>> {
        set_contract_version(deps.storage, CONTRACT_NAME, CONTRACT_VERSION)?;

        let info = ContractInfoResponse {
            name: msg.name,
            symbol: msg.symbol,
        };
        self.contract_info.save(deps.storage, &info)?;
        let minter = deps.api.addr_validate(&msg.minter)?;
        self.minter.save(deps.storage, &minter)?;

        let state = State {
            // mint_denom: "uluna",
            mint_fee: Uint256::zero(),
            max_token_limit: 0,
            token_uris: None,
            whitelist: vec!(),
            enable_whitelist: true,
            limit_per_address: 1,
        };
        STATE.save(deps.storage, &state)?;

        Ok(Response::default())
    }

    pub fn execute(
        &self,
        deps: DepsMut,
        env: Env,
        info: MessageInfo,
        msg: ExecuteMsg<T>,
    ) -> Result<Response<C>, ContractError> {
        match msg {
            ExecuteMsg::Mint(msg) => self.mint(deps, env, info, msg),
            ExecuteMsg::Approve {
                spender,
                token_id,
                expires,
            } => self.approve(deps, env, info, spender, token_id, expires),
            ExecuteMsg::Revoke { spender, token_id } => {
                self.revoke(deps, env, info, spender, token_id)
            }
            ExecuteMsg::ApproveAll { operator, expires } => {
                self.approve_all(deps, env, info, operator, expires)
            }
            ExecuteMsg::RevokeAll { operator } => self.revoke_all(deps, env, info, operator),
            ExecuteMsg::TransferNft {
                recipient,
                token_id,
            } => self.transfer_nft(deps, env, info, recipient, token_id),
            ExecuteMsg::SendNft {
                contract,
                token_id,
                msg,
            } => self.send_nft(deps, env, info, contract, token_id, msg),
            ExecuteMsg::TransferBalance{
                denom
            } => self.transfer_balance(deps.as_ref(), env, info, denom),
            ExecuteMsg::SetMintFee{ amount } => self.set_mint_fee(deps, info, amount),
            ExecuteMsg::BulkSetTokenUris{ token_uris } => self.bulk_set_token_uris(deps, info, token_uris),
            ExecuteMsg::UpdateToken{ token_id, token_uri } => self.update_token(deps, info, token_id, token_uri),
            ExecuteMsg::AddWhitelist{ users } => self.add_whitelist(deps, info, users),
            ExecuteMsg::UpdateEnableWhitelist{ value } => self.update_enable_whitelist(deps, info, value),
            ExecuteMsg::UpdateMaxTokenLimit{ max_token_limit } => self.update_max_token_limit(deps, info, max_token_limit),
            ExecuteMsg::UpdateLimitPerAddress{ limit_per_address } => self.update_limit_per_address(deps, info, limit_per_address),
        }
    }
}

// TODO pull this into some sort of trait extension??
impl<'a, T, C> Cw721Contract<'a, T, C>
where
    T: Serialize + DeserializeOwned + Clone,
    C: CustomMsg,
{
    pub fn mint(
        &self,
        deps: DepsMut,
        _env: Env,
        info: MessageInfo,
        msg: MintMsg<T>,
    ) -> Result<Response<C>, ContractError> {
        let state = STATE.load(deps.storage)?;
        let num_tokens = self.num_tokens(deps.as_ref())?.count;
        let token_id: String = (Uint256::from(num_tokens) + Uint256::one()).to_string();
        // https://github.com/Anchor-Protocol/money-market-contracts/blob/81af144ac6e17bb2c87c88e7ae85d9ff483d4295/contracts/market/src/borrow.rs#L123
        let amount: Uint256 = info
        .funds
        .iter()
        .find(|c| c.denom == "uluna")
        // .find(|c| c.denom == state.mint_denom)
        .map(|c| Uint256::from(c.amount))
        .unwrap_or_else(Uint256::zero);

        if amount < Uint256::from(state.mint_fee) {
            return Err(ContractError::InsufficientMintFee {});
        }
        if u64::from(num_tokens) + 1 > state.max_token_limit {
            return Err(ContractError::ReachedMaxTokenLimit {});
        }
        // theres a whitelist and sender not on it
        if state.enable_whitelist && !state.whitelist.contains(&info.sender.to_string()) {
            return Err(ContractError::NotOnWhitelist {});
        }
        // can not exceed per address limit
        if self.tokens(deps.as_ref(), info.sender.to_string(), None, None)?.tokens.len() >= state.limit_per_address as usize {
            return Err(ContractError::ReachedMaxPerAddressLimit {})
        }

        // create the token
        let token = TokenInfo {
            owner: deps.api.addr_validate(&msg.owner)?,
            approvals: vec![],
            token_uri: None,
            extension: msg.extension,
        };
        self.tokens
            .update(deps.storage, &token_id, |old| match old {
                Some(_) => Err(ContractError::Claimed {}),
                None => Ok(token),
            })?;

        self.increment_tokens(deps.storage)?;

        Ok(Response::new()
            .add_attribute("action", "mint")
            .add_attribute("minter", info.sender)
            .add_attribute("token_id", token_id))
    }

    fn transfer_balance(
        &self,
        deps: Deps,
        env: Env,
        info: MessageInfo,
        denom: String,
    ) -> Result<Response<C>, ContractError> {
        // only minter can transfer balance
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        // let balance = 100;
        let balance = mm_query_balance(deps, env.contract.address, denom.clone()).unwrap();

        // Cannot transfer balance zero amount
        if balance.is_zero() {
            return Err(ContractError::ZeroTransferBalance(denom));
        }

        Ok(Response::new()
            .add_message(CosmosMsg::Bank(BankMsg::Send {
                to_address: info.sender.clone().to_string(),
                amount: vec![Coin {
                        denom: denom.clone(),
                        amount: Uint128::from(balance),
                    }],
            }))
            .add_attribute("sender", info.sender)
            .add_attribute("action", "transfer_balance")
            .add_attribute("denom", denom)
            .add_attribute("amount", balance.to_string())
        )
    }

    fn set_mint_fee(
        &self,
        deps: DepsMut,
        info: MessageInfo,
        amount: Uint256,
    ) -> Result<Response<C>, ContractError> {
        // only minter can set mint fee
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        STATE.update(deps.storage, |mut state| -> Result<_, ContractError>{
            state.mint_fee = amount;
            Ok(state)
        })?;
        Ok(Response::default())
    }

    fn bulk_set_token_uris(
        &self,
        deps: DepsMut,
        info: MessageInfo,
        token_uris: Vec<(String, String)>
    ) -> Result<Response<C>, ContractError> {
        // only minter can set token uris
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        // pass token uris
        STATE.update(deps.storage, |mut state| -> Result<_, ContractError>{
            if token_uris.len() > 0 {
                state.token_uris = Some(token_uris);
            }
            Ok(state)
        })?;

        // loop through and update token info
        let state = STATE.load(deps.storage)?;
        let mut attributes = vec![attr("action", "update_token_uris")];
        for token_uri_struct in state.token_uris.unwrap() {
            let token_id: String = token_uri_struct.0;
            let uri: String = token_uri_struct.1;
            attributes.push(attr("token_id", token_id.to_string()));
            let mut token = self.tokens.load(deps.storage, &token_id)?;
            token.token_uri = Some((&uri).to_string());
            self.tokens.save(deps.storage, &token_id, &token)?;
        }

        Ok(Response::default()
            .add_attribute("sender", info.sender)
            .add_attribute("action", "set token uris")
        )
    }

    
    fn update_token(
      &self,
      deps: DepsMut,
      info: MessageInfo,
      token_id: String, 
      token_uri: Option<String>,
  ) -> Result<Response<C>, ContractError> {
      // only minter can set token uris
      let minter = self.minter.load(deps.storage)?;

      if info.sender != minter {
          return Err(ContractError::Unauthorized {});
      }
      self.tokens.update(deps.storage, &token_id, |token| match token {
        Some(mut token_info) => {
            token_info.token_uri = token_uri;
            Ok(token_info)
        }
        None => return Err(ContractError::TokenNotFound {}),
      })?;
      

      Ok(Response::default()
          .add_attribute("sender", info.sender)
          .add_attribute("action", "updated token")
          .add_attribute("token id", token_id.clone())
      )
  }


    fn add_whitelist(
        &self,
        deps: DepsMut,
        info: MessageInfo,
        mut users: Vec<String>,
    ) -> Result<Response<C>, ContractError> {
        // only minter can update token uris
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        let mut state = STATE.load(deps.storage)?;
        let mut attributes = vec![attr("action", "add_whitelist")];
        state.whitelist.append(&mut users);
        STATE.save(deps.storage, &state)?;
        attributes.push(attr("users", users.join(",")));

        Ok(Response::new().add_attributes(attributes))
    }

    fn update_enable_whitelist(
        &self,
        deps: DepsMut,
        info: MessageInfo,
        value: bool,
    ) -> Result<Response<C>, ContractError> {
        // only minter can update token uris
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        let mut state = STATE.load(deps.storage)?;
        let mut attributes = vec![attr("action", "update_enable_whitelist")];
        state.enable_whitelist = value;
        STATE.save(deps.storage, &state)?;
        attributes.push(attr("value", value.to_string()));

        Ok(Response::new().add_attributes(attributes))
    }

    fn update_max_token_limit(
        &self,
        deps: DepsMut,
        info: MessageInfo,
        max_token_limit: u64,
    ) -> Result<Response<C>, ContractError> {
        // only minter can update max token limit
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        let mut state = STATE.load(deps.storage)?;
        let mut attributes = vec![attr("action", "update_max_token_limit")];
        state.max_token_limit = max_token_limit;
        STATE.save(deps.storage, &state)?;
        attributes.push(attr("max_token_limit", max_token_limit.to_string()));

        Ok(Response::new().add_attributes(attributes))
    }

    fn update_limit_per_address(
        &self,
        deps: DepsMut,
        info: MessageInfo,
        limit_per_address: u64,
    ) -> Result<Response<C>, ContractError> {
        // only minter can update limit per address
        let minter = self.minter.load(deps.storage)?;

        if info.sender != minter {
            return Err(ContractError::Unauthorized {});
        }

        let mut state = STATE.load(deps.storage)?;
        let mut attributes = vec![attr("action", "update_limit_per_address")];
        state.limit_per_address = limit_per_address;
        STATE.save(deps.storage, &state)?;
        attributes.push(attr("limit_per_address", limit_per_address.to_string()));

        Ok(Response::new().add_attributes(attributes))
    }
}

impl<'a, T, C> Cw721Execute<T, C> for Cw721Contract<'a, T, C>
where
    T: Serialize + DeserializeOwned + Clone,
    C: CustomMsg,
{
    type Err = ContractError;

    fn transfer_nft(
        &self,
        deps: DepsMut,
        env: Env,
        info: MessageInfo,
        recipient: String,
        token_id: String,
    ) -> Result<Response<C>, ContractError> {
        self._transfer_nft(deps, &env, &info, &recipient, &token_id)?;

        Ok(Response::new()
            .add_attribute("action", "transfer_nft")
            .add_attribute("sender", info.sender)
            .add_attribute("recipient", recipient)
            .add_attribute("token_id", token_id))
    }

    fn send_nft(
        &self,
        deps: DepsMut,
        env: Env,
        info: MessageInfo,
        contract: String,
        token_id: String,
        msg: Binary,
    ) -> Result<Response<C>, ContractError> {
        // Transfer token
        self._transfer_nft(deps, &env, &info, &contract, &token_id)?;

        let send = Cw721ReceiveMsg {
            sender: info.sender.to_string(),
            token_id: token_id.clone(),
            msg,
        };

        // Send message
        Ok(Response::new()
            .add_message(send.into_cosmos_msg(contract.clone())?)
            .add_attribute("action", "send_nft")
            .add_attribute("sender", info.sender)
            .add_attribute("recipient", contract)
            .add_attribute("token_id", token_id))
    }

    fn approve(
        &self,
        deps: DepsMut,
        env: Env,
        info: MessageInfo,
        spender: String,
        token_id: String,
        expires: Option<Expiration>,
    ) -> Result<Response<C>, ContractError> {
        self._update_approvals(deps, &env, &info, &spender, &token_id, true, expires)?;

        Ok(Response::new()
            .add_attribute("action", "approve")
            .add_attribute("sender", info.sender)
            .add_attribute("spender", spender)
            .add_attribute("token_id", token_id))
    }

    fn revoke(
        &self,
        deps: DepsMut,
        env: Env,
        info: MessageInfo,
        spender: String,
        token_id: String,
    ) -> Result<Response<C>, ContractError> {
        self._update_approvals(deps, &env, &info, &spender, &token_id, false, None)?;

        Ok(Response::new()
            .add_attribute("action", "revoke")
            .add_attribute("sender", info.sender)
            .add_attribute("spender", spender)
            .add_attribute("token_id", token_id))
    }

    fn approve_all(
        &self,
        deps: DepsMut,
        env: Env,
        info: MessageInfo,
        operator: String,
        expires: Option<Expiration>,
    ) -> Result<Response<C>, ContractError> {
        // reject expired data as invalid
        let expires = expires.unwrap_or_default();
        if expires.is_expired(&env.block) {
            return Err(ContractError::Expired {});
        }

        // set the operator for us
        let operator_addr = deps.api.addr_validate(&operator)?;
        self.operators
            .save(deps.storage, (&info.sender, &operator_addr), &expires)?;

        Ok(Response::new()
            .add_attribute("action", "approve_all")
            .add_attribute("sender", info.sender)
            .add_attribute("operator", operator))
    }

    fn revoke_all(
        &self,
        deps: DepsMut,
        _env: Env,
        info: MessageInfo,
        operator: String,
    ) -> Result<Response<C>, ContractError> {
        let operator_addr = deps.api.addr_validate(&operator)?;
        self.operators
            .remove(deps.storage, (&info.sender, &operator_addr));

        Ok(Response::new()
            .add_attribute("action", "revoke_all")
            .add_attribute("sender", info.sender)
            .add_attribute("operator", operator))
    }
}

// helpers
impl<'a, T, C> Cw721Contract<'a, T, C>
where
    T: Serialize + DeserializeOwned + Clone,
    C: CustomMsg,
{
    pub fn _transfer_nft(
        &self,
        deps: DepsMut,
        env: &Env,
        info: &MessageInfo,
        recipient: &str,
        token_id: &str,
    ) -> Result<TokenInfo<T>, ContractError> {
        let mut token = self.tokens.load(deps.storage, &token_id)?;
        // ensure we have permissions
        self.check_can_send(deps.as_ref(), env, info, &token)?;
        // set owner and remove existing approvals
        token.owner = deps.api.addr_validate(recipient)?;
        token.approvals = vec![];
        self.tokens.save(deps.storage, &token_id, &token)?;
        Ok(token)
    }

    #[allow(clippy::too_many_arguments)]
    pub fn _update_approvals(
        &self,
        deps: DepsMut,
        env: &Env,
        info: &MessageInfo,
        spender: &str,
        token_id: &str,
        // if add == false, remove. if add == true, remove then set with this expiration
        add: bool,
        expires: Option<Expiration>,
    ) -> Result<TokenInfo<T>, ContractError> {
        let mut token = self.tokens.load(deps.storage, &token_id)?;
        // ensure we have permissions
        self.check_can_approve(deps.as_ref(), env, info, &token)?;

        // update the approval list (remove any for the same spender before adding)
        let spender_addr = deps.api.addr_validate(spender)?;
        token.approvals = token
            .approvals
            .into_iter()
            .filter(|apr| apr.spender != spender_addr)
            .collect();

        // only difference between approve and revoke
        if add {
            // reject expired data as invalid
            let expires = expires.unwrap_or_default();
            if expires.is_expired(&env.block) {
                return Err(ContractError::Expired {});
            }
            let approval = Approval {
                spender: spender_addr,
                expires,
            };
            token.approvals.push(approval);
        }

        self.tokens.save(deps.storage, &token_id, &token)?;

        Ok(token)
    }

    /// returns true iff the sender can execute approve or reject on the contract
    pub fn check_can_approve(
        &self,
        deps: Deps,
        env: &Env,
        info: &MessageInfo,
        token: &TokenInfo<T>,
    ) -> Result<(), ContractError> {
        // owner can approve
        if token.owner == info.sender {
            return Ok(());
        }
        // operator can approve
        let op = self
            .operators
            .may_load(deps.storage, (&token.owner, &info.sender))?;
        match op {
            Some(ex) => {
                if ex.is_expired(&env.block) {
                    Err(ContractError::Unauthorized {})
                } else {
                    Ok(())
                }
            }
            None => Err(ContractError::Unauthorized {}),
        }
    }

    /// returns true iff the sender can transfer ownership of the token
    fn check_can_send(
        &self,
        deps: Deps,
        env: &Env,
        info: &MessageInfo,
        token: &TokenInfo<T>,
    ) -> Result<(), ContractError> {
        // owner can send
        if token.owner == info.sender {
            return Ok(());
        }

        // any non-expired token approval can send
        if token
            .approvals
            .iter()
            .any(|apr| apr.spender == info.sender && !apr.is_expired(&env.block))
        {
            return Ok(());
        }

        // operator can send
        let op = self
            .operators
            .may_load(deps.storage, (&token.owner, &info.sender))?;
        match op {
            Some(ex) => {
                if ex.is_expired(&env.block) {
                    Err(ContractError::Unauthorized {})
                } else {
                    Ok(())
                }
            }
            None => Err(ContractError::Unauthorized {}),
        }
    }
}
