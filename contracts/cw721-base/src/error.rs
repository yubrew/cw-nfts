use cosmwasm_std::StdError;
use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
pub enum ContractError {
    #[error("{0}")]
    Std(#[from] StdError),

    #[error("Unauthorized")]
    Unauthorized {},

    #[error("token_id already claimed")]
    Claimed {},

    #[error("Cannot set approval that is already expired")]
    Expired {},

    #[error("Transfer balance amount must be greater than 0 {0}")]
    ZeroTransferBalance(String),

    #[error("Insufficient transaction mint fee")]
    InsufficientMintFee {},

    #[error("Reached max token limit. Cannot mint more")]
    ReachedMaxTokenLimit {},

    #[error("Your address not on the whitelist. Cannot mint yet")]
    NotOnWhitelist {},

    #[error("Reached max limit per address. Cannot mint more")]
    ReachedMaxPerAddressLimit {},

    #[error("Token id not found")]
    TokenNotFound {},
}
